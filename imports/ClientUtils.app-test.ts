import { DDP } from 'meteor/ddp-client';
import { Meteor } from 'meteor/meteor';
import { Promise } from 'meteor/promise';
import { AssertionError } from 'chai';

/**
 * Returns a promise which resolves when all subscriptions are done.
 */
export function waitForSubscriptions(): Promise<void> {
	return new Promise((resolve) => {
		const poll = Meteor.setInterval(() => {
			if (DDP._allSubscriptionsReady()) {
				Meteor.clearInterval(poll);
				resolve();
			}
		}, 200);
	});
}

/**
 * Try an assertion in 100ms interval
 * @param assertion function that throws an AssertionError until its demands are met
 * @param timeout after this many milliseconds, the AssertionError is passed on
 * @returns Returns a promise that resolves with the last return value of
 * assertion() once the assertion holds. The promise is
 * rejected when the assertion throws something which is not an AssertionError
 * or when the timeout runs out without the assertion coming through.

 */
export function waitFor<T>(assertion: () => T, timeout = 10000): Promise<T> {
	return new Promise((resolve, reject) => {
		const start = new Date().getTime();
		let interval: false | NodeJS.Timer = false;

		const clearWatchers = () => {
			if (interval) {
				clearInterval(interval);
			}
		};

		const tryIt = () => {
			try {
				const result = assertion();
				clearWatchers();
				resolve(result);
				return true;
			} catch (e) {
				if (e instanceof AssertionError) {
					if (new Date().getTime() - start < timeout) {
						return false;
					}
				}
				clearWatchers();
				reject(e);
			}
			return false;
		};

		if (tryIt()) {
			return;
		}
		interval = setInterval(tryIt, 100);
	});
}
